package mypackage.hr_management.controller;


import mypackage.hr_management.payload.AssignmentDto;
import mypackage.hr_management.payload.template.ApiResponse;
import mypackage.hr_management.service.AssignmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/assignment")
public class AssignmentController {
    @Autowired
    AssignmentService assignmentService;

    @PostMapping("/add")
    public HttpEntity<?> addAssignment(@RequestBody AssignmentDto assignmentDto){
        ApiResponse apiResponse = assignmentService.addAssignment(assignmentDto);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }

}
