package mypackage.hr_management.controller;

import mypackage.hr_management.payload.LoginDto;
import mypackage.hr_management.payload.PasswordDto;
import mypackage.hr_management.payload.RegisterDto;
import mypackage.hr_management.payload.template.ApiResponse;
import mypackage.hr_management.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("api/auth")
public class AuthController {

    @Autowired
    AuthService authService;


// director uchun
    @PostMapping("/register/director")
    public HttpEntity<?> registerUser(@RequestBody RegisterDto registerDto){
        ApiResponse apiResponse = authService.registerUser(registerDto);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }

//    ishchi uchun
    @PostMapping("/register/employee")
    public HttpEntity<?> registerEmployee(@RequestBody RegisterDto registerDto){
        ApiResponse apiResponse = authService.registerEmployee(registerDto);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }




    @PostMapping("/verifyEmail")
    public HttpEntity<?> verifyEmail(@RequestParam String emailCode, @RequestParam String email, @RequestBody PasswordDto passwordDto){
        ApiResponse apiResponse=authService.verifyEmail(emailCode, email, passwordDto);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @PostMapping("/login")
    public HttpEntity<?> login(@RequestBody  LoginDto loginDto){
        ApiResponse apiResponse = authService.login(loginDto);
        return ResponseEntity.status(apiResponse.isSuccess()?200:401).body(apiResponse);
    }


}
