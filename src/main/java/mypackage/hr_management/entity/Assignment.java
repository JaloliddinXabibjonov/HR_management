package mypackage.hr_management.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Assignment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private UUID id;

    @Column(nullable = false)
    private String name;    // topshiriq nomi

    @Column(nullable = false)
    private String comment;  //topshiriqqa izoh

    @Column(nullable = false, length = 1000)
    private Date finishTime;  // topshiriqni yakunlanish vaqti  kun/oy/yil raqamlar bilan kiritiladi


    @Column(nullable = false)
    private String employeeUsername;   // topshiriq qaysi xodimga berilayotganligi (xodimning email i kiritiladi)


    @Column(nullable = false)
    private String ownerAssignment;    //topshiriqni bajarilganligini qabul qilib oluvchining  'email'i kiritiladi

    @Column(nullable = false)
    private boolean status;  // topshiriqni bajarilgan yoki bajarilmaganligi holati


}
