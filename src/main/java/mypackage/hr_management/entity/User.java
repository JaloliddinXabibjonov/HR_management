package mypackage.hr_management.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "users")
public class User implements UserDetails {
    @Id
    @GeneratedValue
    private UUID id;    //userning takrorlanmas kodi

    @Column(nullable = false , length = 50)
    private String firstName; //ismi

    @Column(nullable = false,length = 50)
    private String lastName; //familiyasi


    @Column(unique = true, nullable = false)
    private String email; //userning email pochtasi. Mas: Example@gmail.com

    @Column(nullable = false)
    private String password; //userning paroli

    @Column(nullable = false, updatable = false)  //userni ro`yxatdan o`tgan vaqtini tahrirlay olmaslik uchun
    @CreationTimestamp//bu tizimdagi vaqtni avtomatik set qilib beradi
    private Timestamp createdAt; //userning qachon ro`yxatdan o`tganligi
    @CreationTimestamp
    private Timestamp updatedAt;    //userni qachon oxirgi marta tahrirlanganligi

    @ManyToMany(fetch= FetchType.EAGER)
    private Set<Role> roles;


    private boolean accountNonExpired=true;    // bu userning amal qilish muddati o`tmaganligi
    private boolean accountNonLocked=true;      // bu user bloklanmagan
    private boolean credentialsNonExpired=true;
    private boolean enabled;
    private String emailCode;

    // ----------------BU USERDETAILSNING METODLARI-------------------
    //BU USERNING HUQUQLARI RO`YHATI
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        return this.roles;
    }

    //BU USERNING USERNAME NI QAYTARUVCHI METOD
    @Override
    public String getUsername() {
        return this.email;
    }

    //ACCOUNTNING AMAL QILISH MUDDATI QAYTARADI
    @Override
    public boolean isAccountNonExpired() {

        return this.accountNonExpired;
    }

    //ACCOUNTNI BLOKLANGANLIGI HOLATI
    @Override
    public boolean isAccountNonLocked() {

        return this.accountNonLocked;
    }

    //ACCOUNTNING ISHONCHLILIK MUDDATI TUGAGAN YOKI TUGAMAGANLIGINI QAYTARADI
    @Override
    public boolean isCredentialsNonExpired() {

        return this.credentialsNonExpired;
    }

    //ACCOUNTNING AKTIVLIGINI QAYTARADI
    @Override
    public boolean isEnabled() {

        return this.enabled;
    }


}
