package mypackage.hr_management.payload;

import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data

public class AssignmentDto {

    @NotNull
    private String name;
    @NotNull
    private String comment;
    @NotNull
    private String finishTime;  // topshiriqni yakunlanish vaqti  kun/oy/yil raqamlar bilan kiritiladi
    @Email
    @NotNull
    private String employeeUsername;   // topshiriq qaysi xodimga berilayotganligi (xodimning email i kiritiladi)
    @Email
    @NotNull
    private String ownerAssignment;    //topshiriqni bajarilganligini qabul qilib oluvchining  'email'i kiritiladi

}
