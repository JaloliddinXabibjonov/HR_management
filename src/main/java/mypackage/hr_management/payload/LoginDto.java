package mypackage.hr_management.payload;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
@Data
public class LoginDto {
    @NotNull
    @Email
    private String username; //userning email pochtasi. Mas: Example@gmail.com

    @NotNull
    private String password; //userning paroli

}
