package mypackage.hr_management.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PasswordDto {
    @NotNull
    private String password;   //userning tizimga ornatadigan paroli
    @NotNull
    private String rePassword; //userning tizimga ornatadigan parolining takrori

}
