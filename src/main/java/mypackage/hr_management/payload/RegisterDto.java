package mypackage.hr_management.payload;

import lombok.Data;


import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class RegisterDto {

    @NotNull
    @Size(min=3,max=50)
    private String firstName; //ismi

    @NotNull
    @Size(min=3,max=50)
    private String lastName; //familiyasi

    @NotNull
    @Email
    private String email; //userning email pochtasi. Mas: Example@gmail.com



}
