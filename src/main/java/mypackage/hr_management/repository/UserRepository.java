package mypackage.hr_management.repository;

import mypackage.hr_management.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    boolean existsByEmail(String email);

    Optional<User> findByEmail( String email);
    Optional<User> findByEmailAndEmailCode(String email, String emailCode);

    @Query(value = "select roles_id from users_roles where users_roles.users_id =\n" +
                            "(select id from users where  users.email=:username)", nativeQuery = true)
    Optional<Integer> findRoleIdByUsername(String username);

}
