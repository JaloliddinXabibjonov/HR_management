package mypackage.hr_management.service;

import mypackage.hr_management.entity.Assignment;
import mypackage.hr_management.entity.User;
import mypackage.hr_management.payload.AssignmentDto;
import mypackage.hr_management.payload.template.ApiResponse;
import mypackage.hr_management.repository.AssignmentRepository;
import mypackage.hr_management.repository.UserRepository;
import mypackage.hr_management.service.authority.Authority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;


@Service
public class AssignmentService {
    @Autowired
    JavaMailSender javaMailSender;
    @Autowired
    UserRepository userRepository;
    @Autowired
    AssignmentRepository assignmentRepository;
    public ApiResponse addAssignment(AssignmentDto assignmentDto) {
        Optional<User> optionalUser = userRepository.findByEmail(assignmentDto.getEmployeeUsername());
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();  // bu user topshiriq yuborilayotgan user
            Optional<Integer> roleIdByUsername = userRepository.findRoleIdByUsername(user.getEmail());
            if (roleIdByUsername.isPresent()){
                Integer roleIdWorker = roleIdByUsername.get();
                Authority authority = new Authority();
                String authority1 = authority.getAuthority();
                int roleId = 0;  // bu tizimdagi userning role id si
                if (authority1.equals("DIRECTOR"))
                    roleId = 1;
                if (authority1.equals("MANAGER") || authority1.equals("HR_MANAGER"))
                    roleId = 2;
                if (authority1.equals("EMPLOYEE"))
                    roleId = 3;
                if (roleId>=roleIdWorker){
                    return new ApiResponse("Siz ushbu tizim foydalanuvchisiga vazifa biriktira olmaysiz!!! "+ assignmentDto.getEmployeeUsername(),false);
                }
                Assignment assignment = new Assignment();
                assignment.setName(assignmentDto.getName());
                assignment.setComment(assignmentDto.getComment());
                assignment.setOwnerAssignment(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
                try {
                    String finishTime = assignmentDto.getFinishTime();
                    Date finishDate=new SimpleDateFormat("dd/MM/yyyy").parse(finishTime);
                    assignment.setFinishTime(finishDate);
                    Assignment savedAssignment = assignmentRepository.save(assignment);

                    //vazifa yuborish qismi
                    SimpleMailMessage mailMessage = new SimpleMailMessage();
                    mailMessage.setFrom("Jaloliddin0292@gmail.com");
                    mailMessage.setTo(savedAssignment.getEmployeeUsername());
                    mailMessage.setSubject("Sizga yangi vazifa yuborildi");
                    mailMessage.setText(savedAssignment.toString());
                    javaMailSender.send(mailMessage);

                } catch (ParseException e) {
                    return new ApiResponse("Sana noto`g'ri kiritildi: "+ assignmentDto.getFinishTime(), false);
                }

            }








        }return new ApiResponse("Bunday foydalanuvchi mavjud emas!!! "+assignmentDto.getEmployeeUsername(), false);





    }

    }


