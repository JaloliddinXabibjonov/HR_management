package mypackage.hr_management.service;


import mypackage.hr_management.config.SpringSecurityAuditConfig;
import mypackage.hr_management.entity.User;
import mypackage.hr_management.entity.enums.RoleName;
import mypackage.hr_management.payload.LoginDto;
import mypackage.hr_management.payload.PasswordDto;
import mypackage.hr_management.payload.RegisterDto;
import mypackage.hr_management.payload.template.ApiResponse;
import mypackage.hr_management.repository.RoleRepository;
import mypackage.hr_management.repository.UserRepository;
import mypackage.hr_management.security.JwtProwider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

@Service
public class AuthService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    JavaMailSender javaMailSender;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    JwtProwider jwtProwider;
    @Autowired
    SpringSecurityAuditConfig springSecurityAuditConfig;
    //DIRECTOR ni ro`yxatdan o`tkazish uchun
    public ApiResponse registerUser(RegisterDto registerDto) {
        boolean existsByEmail = userRepository.existsByEmail(registerDto.getEmail());
        if (existsByEmail)
            return new ApiResponse("Bunday email bilan oldin ro`yxatdan o`tilgan", false);
        User user = new User();
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setEmail(registerDto.getEmail());
        user.setPassword("null");
        user.setRoles(Collections.singleton(roleRepository.findByRoleName(RoleName.DIRECTOR)));
        user.setEmailCode(UUID.randomUUID().toString());
        userRepository.save(user);

        //EMAILGA YUBORISH METODINI CHAQIRAMIZ
        sendEmail(user.getEmail(), user.getEmailCode());
        return new ApiResponse("Muvaffaqiyatli ro`yxatdan o`tdingiz, akkountingizni aktivlashtirish uchun emailingizni tasdiqlang", true);

    }


    // EMPLOYEE ni ro`yxatdan o`tkazish uchun
    public ApiResponse registerEmployee(RegisterDto registerDto){
        boolean existsByEmail = userRepository.existsByEmail(registerDto.getEmail());
        if (existsByEmail)
            return new ApiResponse("Bunday email bilan oldin ro`yxatdan o`tilgan", false);
        User user = new User();
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setEmail(registerDto.getEmail());
        user.setPassword("null");
        user.setRoles(Collections.singleton(roleRepository.findByRoleName(RoleName.EMPLOYEE)));
        user.setEmailCode(UUID.randomUUID().toString());
        userRepository.save(user);

        //EMAILGA YUBORISH METODINI CHAQIRAMIZ
        sendEmail(user.getEmail(), user.getEmailCode());
        return new ApiResponse("Muvaffaqiyatli ro`yxatdan o`tdingiz, akkountingizni aktivlashtirish uchun emailingizni tasdiqlang", true);

    }

    public Boolean sendEmail(String sendingEmail, String emailCode) {
        try {
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setFrom("Jaloliddin0292@gmail.com");
            mailMessage.setTo(sendingEmail);
            mailMessage.setSubject("Akkountni tasdiqlash");
            mailMessage.setText("http://localhost:9090/api/auth/verifyEmail?emailCode=" + emailCode + "&email=" + sendingEmail);
            javaMailSender.send(mailMessage);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public ApiResponse verifyEmail(String emailCode, String email, PasswordDto passwordDto) {
        Optional<User> optionalUser = userRepository.findByEmailAndEmailCode(email, emailCode);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            if (passwordDto.getPassword().equals(passwordDto.getRePassword())){
                user.setPassword(passwordEncoder.encode(passwordDto.getPassword()));
            }else{
                return new ApiResponse("Kiritilgan parol va takroriy parollar bir xil emas!", false);
            }
            user.setEnabled(true);
            user.setEmailCode(null);
            userRepository.save(user);
            return new ApiResponse("Akkount muvaffaqiyatli tasdiqlandi va parol o`rnatildi", true);
        }
        return new ApiResponse("Akkount allaqachon tasdiqlangan", false);
    }

    public ApiResponse login(LoginDto loginDto) {
        try {
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    loginDto.getUsername(),
                    loginDto.getPassword()));
            User user = (User) authentication.getPrincipal();
            String token = jwtProwider.generateToken(loginDto.getUsername(), user.getRoles());
            return new ApiResponse("Token", true, token);
        } catch (BadCredentialsException badCredentialsException) {
return new ApiResponse("Parol yoki login xato!", false);
        }
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        Optional<User> optionalUser = userRepository.findByEmail(username);
//        if (optionalUser.isPresent())
//            return optionalUser.get();
//        throw new UsernameNotFoundException(username+" topilmadi");
        User user = userRepository.findByEmail(username).orElseThrow(() -> new UsernameNotFoundException(username + " topilmadi"));
        return user;
    }
}
