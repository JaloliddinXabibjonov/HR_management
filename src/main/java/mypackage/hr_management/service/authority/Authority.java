package mypackage.hr_management.service.authority;

import lombok.*;
import mypackage.hr_management.entity.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collection;


@Data
@AllArgsConstructor
public class Authority {
    public String getAuthority() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null &&
                authentication.isAuthenticated() &&
                !authentication.getPrincipal().equals("anonymousUser")) {
            Collection<? extends GrantedAuthority> authorities = ((User) authentication.getPrincipal()).getAuthorities();
            for (GrantedAuthority authority : authorities) {
                String authority1 = authority.getAuthority();
                return authority1;
            }

        }
        return null;
    }


}
